#!/usr/bin/env python

# Тази програма преобразува CSV файловете с подобни задачи, генерирани от Maxima, в XML файлове.
# Трябва да бъде извикана с два входни параметъра:
# входния CSV файл и изходния XML файл

# трябва да e инсталиранa pandas
import sys
import pandas
import re
from py_asciimath.translator.translator import (
    ASCIIMath2Tex
)
asciimath2tex = ASCIIMath2Tex(log=False, inplace=True)

# прочитаме имената на двата файла, подадени при извикването на тази програма
infile_name = sys.argv[1]
outfile_name = sys.argv[2]
zadacha_data = sys.argv[3]
zadacha_nomer = sys.argv[4]

def translate_asciimath(mathobj):
    orgnl = mathobj.group(0)[1:-1]
    trnsl = asciimath2tex.translate(orgnl)
    # замени доларите с наклонени черти и скоби,
    # за да може знака за долар да бъде ползван
    # в условията на задачи при нужда
    # и защото MathJax го отчита само ако е двоен,
    # но работи добре с отваряща и затваряща скоба
    return "\(" + trnsl[1:-1] + "\)"

def translate_line(line):
    return re.sub('`.*?`', translate_asciimath, line)

# записваме ги в изходния XML файл
xml_preamb = '<?xml version="1.0" encoding="UTF-8"?>\n'

with open(outfile_name, "w") as outfile:
  outfile.write(xml_preamb)
  outfile.write("<ekzamenoj>\n")
  outfile.write("  <ekzameno>\n")
  outfile.write("    <dato>" + zadacha_data + "</dato>\n")
  outfile.write("    <problemoj>\n")
  outfile.write("      <problemo>\n")
  outfile.write("        <numero>" + zadacha_nomer + "</numero>\n")
  outfile.write("        <similaj>\n")
  
  zadachi = pandas.read_csv(infile_name)
  # Вземаме последните две колони от таблицата
  # За целта, транспонираме матрицата с атрибута ѝ T,
  # след това вземаме последните два реда (които преди това са били колони)
  # и отново транспонираме матрицата, за да се върне в първоначалния си вид
  # Има и други начини, но при никой от тях не успях да взема последната колона,
  # защото всички те изглежда че изключваха последната колона в обхвата,
  # който съм им задал, а нямаше начин да задам колона след последната.
  last_columns = zadachi.T.tail(2).T
  for row in last_columns.itertuples():
    uslovie_asciimath = str(row.условие)
    otgovor_asciimath = str(row.отговор)
    uslovie_latex = translate_line(uslovie_asciimath)
    otgovor_latex = translate_line(otgovor_asciimath)
    outfile.write("        <problemo>\n")
    outfile.write("          <asciimath>\n")
    outfile.write("            <kondiĉo>" + uslovie_asciimath + "</kondiĉo>\n")
    outfile.write("            <respondo>" + otgovor_asciimath + "</respondo>\n")
    outfile.write("          </asciimath>\n")
    outfile.write("          <latex>\n")
    outfile.write("            <kondiĉo>" + uslovie_latex + "</kondiĉo>\n")
    outfile.write("            <respondo>" + otgovor_latex + "</respondo>\n")
    outfile.write("          </latex>\n")
    outfile.write("        </problemo>\n")
  outfile.write("        </similaj>\n")
  outfile.write("      </problemo>\n")
  outfile.write("    </problemoj>\n")
  outfile.write("  </ekzameno>\n")
  outfile.write("</ekzamenoj>\n")
