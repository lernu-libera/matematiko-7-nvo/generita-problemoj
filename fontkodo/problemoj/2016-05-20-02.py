#!/usr/bin/env python

import sys
import csv
import random

def is_dict_unique(d, mat):
  for m in mat:
    if d == m:
      return False
  return True
  
def print_matrix(mat):
  for m in mat:
    print(m)
    
def gena():
  # Задължително трябва долният праг да е по-голям
  # от долния праг на другата променлива.
  # В противен случай, влизаме в безкраен цикъл,
  # който търси по-малка стойност от минималната.
  return random.randint(20, 100)
      
def genb(p):
  while True:
    t = random.randint(10, 100)
    if t < p:
      return t
      
mat = []
i = 0

while True:
  a = gena()
  b = genb(a)
  tempdict = {
    "a": a,
    "b": b
  }
  if is_dict_unique(tempdict, mat):
    mat.insert(i, tempdict)
    i += 1
  if i == 100:
    break

# Вземаме пътя до файла, който сме подали при извикването на този скрипт
filename = str(sys.argv[1])
print(filename)
with open(filename, 'w', newline='') as csvfile:
  fieldnames = ['a', 'b']
  writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

  writer.writeheader()
  for row in mat:
    writer.writerow(row)