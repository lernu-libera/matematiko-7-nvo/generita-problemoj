#!/usr/bin/env python

import sys
import csv
import random

def is_dict_unique(d, mat):
  for m in mat:
    if d == m:
      return False
  return True
  
def print_matrix(mat):
  for m in mat:
    print(m)
    
def gena():
  return random.randint(100, 1000)
      
def genb():
  return random.randint(2, 9)
      
def genc():
  return random.randint(10, 100)
      
def gend():
  return random.choice([-1, 1])
      
mat = []
i = 0

while True:
  tempdict = {
    "a": gena(),
    "b": genb(),
    "c": genc(),
    "d": gend()
  }
  if is_dict_unique(tempdict, mat):
    mat.insert(i, tempdict)
    i += 1
  if i == 100:
    break

# Вземаме пътя до файла, който сме подали при извикването на този скрипт
filename = str(sys.argv[1])
print(filename)
with open(filename, 'w', newline='') as csvfile:
  fieldnames = ['a', 'b', 'c', 'd']
  writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

  writer.writeheader()
  for row in mat:
    writer.writerow(row)