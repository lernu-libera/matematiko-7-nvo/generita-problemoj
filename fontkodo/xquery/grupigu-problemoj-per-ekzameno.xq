declare option output:omit-xml-declaration "no";
<ekzamenoj>{
  for $problemoj in /problemoj/problemo
  let $dato := data($problemoj/dato)
  group by $dato
  order by $dato
  return
  <ekzameno>
    <dato>{$dato}</dato>
    <problemoj>{
      for $problemo in $problemoj
      let $numero := data($problemo/numero)
      order by $numero
      return
      <problemo>
        {$problemo/numero}
        {$problemo/similaj}
      </problemo>
    }</problemoj>
  </ekzameno>
}</ekzamenoj>
